# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/src" ] kde [ translations=qt ]

export_exlib_phases src_prepare src_test

SUMMARY="A database connectivity and creation framework"
DESCRIPTION="
It consists of a general-purpose C++ Qt library and set of plugins delivering
support for various database vendors."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 LGPL-2"
SLOT="0"
MYOPTIONS="
    doc
    ( mysql postgresql sqlite ) [[ number-selected = at-least-one ]]
"

KF5_MIN_VER=5.16.0

DEPENDENCIES="
    build:
        dev-lang/python:2.7
        virtual/pkg-config [[ note = [ sqlite and postgresql ] ]]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*
            x11-libs/qttools:5 [[ note = [ qhelpgenerator ] ]]
        )
    build+run:
        dev-libs/icu:=
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.4.0][gui]
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        sqlite? (
            dev-db/sqlite:3[>=3.6.16] [[ note = [ 3.27.1 is recommended ] ]]
        )
"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_EXAMPLES:BOOL=FALSE )

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    MySQL
    PostgreSQL
    Sqlite
)

kdb_src_prepare() {
    cmake_src_prepare

    if ! option sqlite ; then
        # The following tests need the sqlite option to work, but we can't
        # depend on it, so we just skip them if the option is disabled.
        edo sed \
            -e '/ConnectionTest.cpp/d' \
            -e '/DriverTest.cpp/d' \
            -e '/MissingTableTest.cpp/d' \
            -e '/OrderByColumnTest.cpp/d' \
            -e '/QuerySchemaTest.cpp/d' \
            -e '/KDbTest.cpp/d' \
            -e '/^target_compile_definitions(MissingTableTest/d' \
            -e '/^target_compile_definitions(OrderByColumnTest/d' \
            -i autotests/CMakeLists.txt
        edo sed -e '/^ecm_add_test(/,+10d' -i autotests/parser/CMakeLists.txt
    fi
}

kdb_src_test() {
    default

    if ! option sqlite ; then
        ewarn "Some tests need the sqlite option enabled and are currently"
        ewarn "skipped because it is disabled. Enable it to include the"
        ewarn "additional tests."
    fi
}

