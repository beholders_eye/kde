# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require option-renames [ renames=[ 'v4l2 v4l' ] ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE Non Linear Video Editor"
HOMEPAGE="https://www.${PN}.org"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/discover/${PV}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    freesound.org [[ description = [ Download sounds from freesound.org
                                     (needs QtWebkit) ] ]]
    v4l [[ description = [ V4L2 device capture support, e.g. for webcams ] ]]
"

KF5_MIN_VER="5.45.0"
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        dev-libs/rttr[>=0.9.6]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media/ffmpeg[player]
        media/mlt[>=6.20.0][ffmpeg][sdl][qt5][xml(+)]
        x11-dri/glu
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=0.23]
        freesound.org? ( x11-libs/qtwebkit:5[>=5.2.0] )
        v4l? ( media-libs/v4l-utils )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 colors ] ]]
        x11-libs/qtquickcontrols:5[>=5.2.0]
    recommendation:
        media-plugins/frei0r-plugins [[
            description = [ Provide additional effects and transitions ]
        ]]
    suggestion:
        media/dvdauthor [[ description = [ Required for the DVD assistant of Kdenlive ] ]]
"

# Besides the (pretty useless to us) appstreamtest the only test needs
# a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR:PATH="/usr/share/doc/${PNVR}"
)
ever at_least scm || CMAKE_SRC_CONFIGURE_PARAMS+=( -DRELEASE_BUILD:BOOL=TRUE )

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'freesound.org Qt5WebKitWidgets'
    'v4l LibV4L2'
)

kdenlive_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdenlive_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

