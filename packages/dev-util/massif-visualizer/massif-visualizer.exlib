# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] kde [ translations="ki18n" ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A visualizer for Valgrind Massif data files"
DESCRIPTION="
Massif Visualizer is a tool that - who'd guess that - visualizes massif data.
You run your application in Valgrind with --tool=massif and then open the
generated massif.out.%pid in the visualizer. Gzip or Bzip2 compressed massif
files can also be opened transparently."

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    callgraph [[
        description = [ View callgraph dot file in the KGraphViewer part ]
    ]]
"

QT_MIN_VER="5.2.0"

DEPENDENCIES="
    build+run:
        kde/kdiagram[>=2.6.0]
        kde-frameworks/karchive:5
        kde-frameworks/kconfig:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/kparts:5
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        callgraph? ( media-gfx/kgraphviewer[>=2.3.90] )
    run:
        dev-util/valgrind
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'callgraph KGraphViewerPart'
)

massif-visualizer_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

massif-visualizer_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

