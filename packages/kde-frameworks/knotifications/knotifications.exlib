# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ] test-dbus-daemon

SUMMARY="KDE Desktop notifications framework"
DESCRIPTION="
KNotification is used to notify the user of an event. It covers feedback and
persistent events.
"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    LGPL-2.1
"
MYOPTIONS="
    canberra [[ description = [ Use libcanberra to play event sounds ] ]]
    phonon   [[ description = [ Use phonon to play event sounds ] ]]
    tts      [[ description = [ Support for text to speech ] ]]
    X   [[ presumed = true ]]

    ( canberra phonon ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/dbusmenu-qt[>=0.9.3_p259][qt5(+)]
        canberra? ( media-libs/libcanberra )
        phonon? ( media-libs/phonon[>=4.6.60][qt5(+)] )
        tts? ( x11-libs/qtspeech:5 )
        X? (
            x11-libs/libX11 [[ note = [ Could make this optional if necessary ] ]]
            x11-libs/libXtst [[ note = [ Could make this optional if necessary ] ]]
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}] [[ note = [ Could make this optional if necessary ] ]]
        )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    Canberra
    Phonon
    'tts Qt5TextToSpeech'
    'X X11'
)

