# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ] freedesktop-mime

export_exlib_phases src_prepare src_test

SUMMARY="Addons to QtCore"
DESCRIPTION="
They perform various tasks such as manipulating mime types, autosaving files,
creating backup files, generating random sequences, performing text
manipulations such as macro replacement, accessing user information and many
more."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        x11-misc/shared-mime-info[>=1.3]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed for migrating settings from ~/.kde4 to ~/.config
    -D_KDE4_DEFAULT_HOME_POSTFIX=4
    # Disable deprecated/obsolete FAM support in favor of using inotify directly
    -DCMAKE_DISABLE_FIND_PACKAGE_FAM:BOOL=TRUE
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
    # procstat is a FreeBSD thing
    -DENABLE_PROCSTAT:BOOL=FALSE
)

kcoreaddons_src_prepare() {
    kde_src_prepare

    # https://bugs.kde.org/show_bug.cgi?id=339117
    edo sed -e '/kshelltest/d' \
            -i autotests/CMakeLists.txt

    # Disable two tests which fail as of 5.44.0
    edo sed -e '/set(KDIRWATCH_BACKENDS_TO_TEST/ s/Stat//' \
            -e 's/HAVE_QFILESYSTEMWATCHER/false/' \
            -i autotests/CMakeLists.txt
}

kcoreaddons_src_test() {
    esandbox allow_net 'unix:/tmp/fam-paludisbuild/fam-*'
    default
    esandbox disallow_net 'unix:/tmp/fam-paludisbuild/fam-*'
}

