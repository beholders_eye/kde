# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Integration with X11 and window managers"
DESCRIPTION="
Convenience access to certain properties and features of the window manager.

KWindowSystem provides information about the state of the window manager and
allows asking the window manager to change the using a more high-level
interface than the NETWinInfo/NETRootInfo low-level classes."

LICENCES="LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-libs/libXfixes
            x11-libs/libXrender
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
            x11-utils/xcb-util-keysyms
            x11-utils/xcb-util-wm
        )
"

# Tests fail
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # "Disable deprecated API including QWidget usage, so that KWindowSystem
    # does not link to QtWidgets. Warning: this is binary and source
    # incompatible."
    -DKWINDOWSYSTEM_NO_WIDGETS:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'X X11'
    'X XCB'
)

kwindowsystem_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

