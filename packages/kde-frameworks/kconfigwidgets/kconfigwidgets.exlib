# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Widgets for configuration dialogs"
DESCRIPTION="
KConfigWidgets provides easy-to-use classes to create configuration dialogs, as
well as a set of widgets which uses KConfig to store their settings.
"

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS="
    designer [[ description = [ Install Qt designer plugins ] ]]
"

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc KF5DocTools' )

kconfigwidgets_src_test() {
    xdummy_start

    default

    xdummy_stop
}

