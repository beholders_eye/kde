# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] test-dbus-daemon
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop freedesktop-mime

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Colletion of Input/Output protocols for KDE"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
MYOPTIONS="
    audio-thumbnail [[ description = [ Thumbnails for audio files from embedded album covers ] ]]
    cursor-thumbnailer [[ description = [ Support for XCursor thumbnails ] ]]
    mtp     [[ description = [ Support for the mtp:/ kioslave accessing MTP devices ] ]]
    nfs     [[ description = [ Support for the NFS kioslave ] ]]
    openexr [[ description = [ Support for OpenEXR thumbnails ] ]]
    samba   [[ description = [ Support for the smb:/ kioslave for MS windows shares ] ]]
    sftp    [[ description = [ Support for the sftp:/ kioslave by libssh ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER=5.66.0
QT_MIN_VER=5.11.0

DEPENDENCIES="
    build:
        dev-util/gperf [[ note = [ man kioslave ] ]]
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        mtp? ( virtual/pkg-config )
        nfs? ( virtual/pkg-config )
        samba? ( virtual/pkg-config )
    build+run:
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}] [[ note = [ possibly optional ] ]]
        kde-frameworks/kactivities-stats:5[>=5.62.0] [[ note = [ possibly optional ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpty:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.6.60][qt5(+)]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=0.40]
        audio-thumbnail? ( media-libs/taglib[>=1.11] )
        cursor-thumbnailer? ( x11-libs/libXcursor )
        mtp? (
            media-libs/libmtp[>=1.1.2]
            !kde/kio-mtp:4 [[
                description = [ kio-mtp was merge into kio-extras ]
                resolution = uninstall-blocked-after
            ]]
        )
        nfs? ( net-libs/libtirpc )
        openexr? ( media-libs/openexr )
        samba? (
            net-fs/samba:=
            net-libs/kdsoap-ws-discovery-client
        )
        sftp? ( net-libs/libssh[>=0.7.0] )
        !kde-frameworks/kactvities:5[<5.20.0] [[
            description = [ Parts of kactivities have been moved to kio-extras ]
            resolution = uninstall-blocked-after
        ]]
        !kde-frameworks/kio:5[<5.3.0] [[
            description = [ kio_trash was moved from kio-extras to kio ]
            resolution = uninstall-blocked-after
        ]]
    run:
        mtp? ( kde/kde-cli-tools:4 [[ note = [ kioclient5 ] ]] )
"

# TODO: Figure out how to start an ioslave under sydbox
RESTRICT=test

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_KDSoapWSDiscoveryClient:BOOL=OFF
    # Support for AppImage thumbnails
    -DCMAKE_DISABLE_FIND_PACKAGE_libappimage:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'audio-thumbnail Taglib'
    'cursor-thumbnailer X11'
    Mtp
    'nfs TIRPC'
    OpenEXR
    Samba
    'sftp libssh'
)

kio-extras_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

kio-extras_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

kio-extras_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

