# Copyright 2013 Ingmar Vanhassel
# Copyright 2013-2014, 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdenetwork.exlib', which is:
#     Copyright 2008-2009, 2010, 2011, 2012 Ingmar Vanhassel
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_configure src_test pkg_postinst pkg_postrm

SUMMARY="Multi-protocol instant messaging client with various plugins for extra functionality"
DESCRIPTION="It supports AIM, ICQ, Jabber, Gadu-Gadu, Novell GroupWise
Messenger, and more. It is designed to be a flexible and extensible
multi-protocol system suitable for personal and enterprise use."

LICENCES="
    FDL-1.2  [[ note = documentation ]]
    GPL-2    [[ note = GUI ]]
    LGPL-2.1 [[ note = libkopete ]]
"

MY_IM_PROTOCOLS+="
    gadu       [[ description = [ Enable support for the Gadu-Gadu protocol ] ]]
    groupwise  [[ description = [ Enable Novell GroupWise Messenger protocol ] ]]
    ilbc       [[ description = [ iLBC codec support for voice calls over Jabber ]
                  requires = [ im_protocols: jingle ] ]]
    jingle     [[ description = [ Voice calls over Jabber including Google Talk ]
                  requires = [ im_protocols: xmpp ] ]]
    meanwhile  [[ description = [ Enable Sametime protocol ] ]]
    xmpp       [[ description = [ Enable Kopete Jabber protocol ] ]]
"

MY_KOPETE_PLUGINS+="
    nowlistening [[ description = [ Displays the song you currently listen to in your status ] ]]
    otr          [[ description = [ Off-the-Record encryption plugin for Kopete ] ]]
    statistics   [[ description = [ Statistics plugin for Kopete ] ]]
    webpresence  [[ description = [ WebPresence plugin for Kopete ] ]]
"
# Fail to build currently (18.04.2), also see below
# im_protocols
# kopete:plugins
#    cryptography [[ description = [ Send and receive encrypted or signed messages ] ]]
#    history2     [[ description = [ New SQLite based history plugin ] ]]
# Depends on im_protocols:jingle

MYOPTIONS="
    im_protocols: ( ${MY_IM_PROTOCOLS} )
    kopete_plugins: ( ${MY_KOPETE_PLUGINS} )

    im_protocols:jingle? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"

KF5_MIN_VER="5.25.0"
QT_MIN_VER="5.6.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        app-crypt/gpgme[>=1.8.0]
        kde/libkleo[>=16.12.0] [[ note = [ aka 5.4.0 ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}]
        kde-frameworks/kemoticons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5(+)]
        media-libs/v4l-utils
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXScrnSaver
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]

        im_protocols:gadu? ( net-im/libgadu[>=1.8.0] )
        im_protocols:groupwise? ( app-crypt/qca:2[>=2.1.0][qt5(+)] )
        im_protocols:jingle? (
            dev-libs/expat
            dev-libs/jsoncpp:=
            media-libs/mediastreamer[>=2.11]
            net-libs/libsrtp[>1.4.4]
            net-libs/ortp[>=0.13]
            sys-sound/alsa-lib
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
        im_protocols:meanwhile? (
            dev-libs/glib:2
            net-libs/meanwhile
        )
        im_protocols:xmpp? (
            app-crypt/qca:2[>=2.1.0][qt5(+)]
            net-dns/libidn
        )
        kopete_plugins:nowlistening? ( dev-libs/glib:2 )
        kopete_plugins:otr? ( net-libs/libotr[>=4.0.0] )
        kopete_plugins:statistics? ( dev-db/sqlite:3 )
        kopete_plugins:webpresence? (
            dev-libs/libxml2:2.0
            dev-libs/libxslt
        )
    run:
        im_protocols:jingle? ( media-libs/speex )
        im_protocols:ilbc? ( media-plugins/msilbc )
    suggestion:
        im_protocols:xmpp? (
            app-crypt/qca:2 [[ description = [ Necessary to make SSL secured xmpp connections with kopete ]
                               note = [ qca-ossl plugin ] ]]
        )
"
# Fail to build currently (18.04.2)
# build+run:
#        kopete_plugins:history2? ( dev-db/sqlite:3 )
#        kopete_plugins:cryptography? ( kde/kleopatra )

kopete_src_configure() {
    local cmakeparams+=(
        -DCMAKE_DISABLE_FIND_PACKAGE_Xmms:BOOL=TRUE

        # All below fail to build currently (18.04.2)
        -DCMAKE_DISABLE_FIND_PACKAGE_Kleopatra:BOOL=TRUE
        -DWITH_cryptography:BOOL=FALSE

        -DWITH_history2:BOOL=FALSE
    )

    # Kopete plugins with no extra dependencies
    # pipes - currently (18.04.2) fails to build
    my_kopete_plugins=(
        addbookmarks autoreplace contactnotes highlight history latex
        nowlistening privacy texteffect translator urlpicpreview
    )

    # Kopete protocols with no extra dependencies
    # skype and sms - currently (18.04.2) fail to build
    my_kopete_protocols=( bonjour oscar qq testbed winpopup )
    for p in "${my_kopete_plugins[@]}" "${my_kopete_protocols[@]}"; do
        cmakeparams+=( -DWITH_${p}:BOOL=TRUE )
    done

    cmakeparams+=(
        $(cmake_disable_find kopete_plugins:statistics Sqlite)
        $(cmake_with im_protocols:gadu)
        $(cmake_disable_find im_protocols:gadu Libgadu)
        $(cmake_with im_protocols:groupwise)
        $(cmake_disable_find im_protocols:jingle Alsa)
        $(cmake_disable_find im_protocols:jingle Expat)
        $(cmake_disable_find im_protocols:jingle Mediastreamer)
        $(cmake_disable_find im_protocols:jingle LiboRTP)
        $(cmake_disable_find im_protocols:jingle OpenSSL)
        $(cmake_disable_find im_protocols:jingle Speex)
        $(cmake_disable_find im_protocols:jingle SRTP)
        $(cmake_with im_protocols:jingle libjingle)
        $(cmake_with im_protocols:meanwhile)
        $(cmake_disable_find im_protocols:meanwhile GLIB2)
        $(cmake_disable_find im_protocols:meanwhile LibMeanwhile)
        $(cmake_disable_find im_protocols:xmpp IDN)
        $(cmake_with im_protocols:xmpp jabber)
        $(cmake_with kopete_plugins:nowlistening)
        $(cmake_disable_find kopete_plugins:otr LibOTR)
        $(cmake_with kopete_plugins:webpresence)
        $(cmake_disable_find kopete_plugins:webpresence LibXml2)
        $(cmake_disable_find kopete_plugins:webpresence LibXslt)
    )

    ecmake $(kf5_shared_cmake_params) "${cmakeparams[@]}"
}

kopete_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

kopete_pkg_postinst()  {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kopete_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

