# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

export_exlib_phases src_install

SUMMARY="System information center"
DESCRIPTION="The infocenter provides you with a centralized and convenient
overview of your system and desktop environment.
The information center is made up of multiple modules.  Each module is a
separate application, but the information center organizes all of these
programs into a convenient location."

LICENCES="FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    ieee1394
    opengles [[ description = [ Use the OpenGLES backend originally targeted at mobile devices, disables the OpenGL backend ] ]]
    wayland"

KF5_MIN_VER="5.66.0"
if ever at_least 5.18.90 ; then
    QT_MIN_VER="5.14.0"
else
    QT_MIN_VER="5.12.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        sys-apps/pciutils
        x11-dri/glu
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXfixes
        x11-libs/qtbase:5[>=${QT_MIN_VER}][opengles=]
        ieee1394? ( media-libs/libraw1394 )
        wayland? ( kde-frameworks/kwayland:5 )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=5.4.0]
    recommendation:
        sys-apps/upower [[ description = [ Display information about wakeups and batteries ] ]]
"

if ever at_least 5.18.90 ; then
    DEPENDENCIES+="
        run:
            kde/systemsettings:4
    "
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
            kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
            kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
            kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'ieee1394 RAW1394'
    OpenGLES
    'wayland KF5Wayland'
)

kinfocenter_src_install() {
    cmake_src_install

    # Install a logo to display on the "About system" KCM
    insinto /etc/xdg
    doins "${FILES}"/kcm-about-distrorc
    insinto /usr/share/kinfocenter
    doins "${FILES}"/zebrapig-logo.png
}

