# Copyright 2015, 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ translations='qt' ]

SUMMARY="A library for creating business charts"
DESCRIPTION="
Besides having all the standard features, it enables the developer to design
and manage a large number of axes and provides sophisticated customization.
KD Chart utilizes the Qt Model-View programming model and allows for re-use of
existing data models to create charts. KD Chart is a complete implementation
of the ODF (OpenDocument) Chart specification. It now includes Stock Charts,
Box & Whisker Charts and the KD Gantt module for implementing ODF Gantt charts
into applications.
This is a KDE-fied version of the one offered by KDAB which has no public
releases, see http://lists.kde.org/?l=kde-core-devel&m=142335191017238&w=2
for details."

HOMEPAGE+=" https://www.kdab.com/kdab-products/kd-chart/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        x11-libs/qttools:5
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        x11-libs/qtbase:5[>=5.6.0][sql]
        x11-libs/qtsvg:5[>=5.6.0]
"

# 19 of 19 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

