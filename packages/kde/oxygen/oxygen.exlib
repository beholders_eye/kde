# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="Qt5-based oxygen style"

LICENCES="GPL-2 LGPL-2.1 LGPL-3"
SLOT="4"
MYOPTIONS="
    X    [[ description = [ Support for displaying the Oxygen style on X11 ] ]]
"

if ever at_least 5.18.90 ; then
    KF5_MIN_VER="[>=5.66.0]"
    QT_MIN_VER="[>=5.14.0]"
fi

DEPENDENCIES="
    build+run:
        kde/kdecoration:4
        kde-frameworks/frameworkintegration:5${KF5_MIN_VER}
        kde-frameworks/kcmutils:5${KF5_MIN_VER}
        kde-frameworks/kcompletion:5${KF5_MIN_VER}
        kde-frameworks/kconfig:5${KF5_MIN_VER}
        kde-frameworks/kconfigwidgets:5${KF5_MIN_VER}
        kde-frameworks/kcoreaddons:5${KF5_MIN_VER}
        kde-frameworks/kguiaddons:5${KF5_MIN_VER}
        kde-frameworks/ki18n:5${KF5_MIN_VER}
        kde-frameworks/kservice:5${KF5_MIN_VER}
        kde-frameworks/kwayland:5${KF5_MIN_VER}
        kde-frameworks/kwidgetsaddons:5${KF5_MIN_VER}
        kde-frameworks/kwindowsystem:5${KF5_MIN_VER}
        x11-libs/qtdeclarative:5${QT_MIN_VER}
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-libs/qtx11extras:5${QT_MIN_VER}
        )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
"

if ever at_least 5.18.90 ; then
    :
else
    CMAKE_SRC_CONFIGURE_PARAMS+=( -DUSE_KDE4:BOOL=FALSE )
fi
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X XCB' )

