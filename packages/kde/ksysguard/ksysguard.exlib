# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache freedesktop-desktop

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A network enabled task and system monitor"
DESCRIPTION="
It features a client/server architecture that allows monitoring of local as
well as remote hosts. The graphical front end uses so-called sensors to
retrieve the information it displays. A sensor can return simple values or
more complex information like tables. For each type of information, one or
more displays are provided. Displays are organized in worksheets that can be
saved and loaded independently from each other. So, ksysguard is not only a
simple task manager but also a very powerful tool to control large server
farms."

LICENCES="FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    lm_sensors
    network-usage [[ description = [ Processes report their network usage statistics ] ]]
"

if ever at_least 5.19.90 ; then
    MYOPTIONS+="
        networkmanager [[ description = [ Provides an improved backend for network statistics ] ]]
    "
fi

KF5_MIN_VER="5.58.0"
if ever at_least 5.18.90 ; then
    QT_MIN_VER=5.14.0
else
    QT_MIN_VER=5.12.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libksysguard:${SLOT}[>=${PV}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        lm_sensors? ( sys-apps/lm_sensors )
        network-usage? (
            dev-libs/libpcap
            sys-libs/libcap
        )
"

if ever at_least 5.19.90 ; then
    DEPENDENCIES+="
        build+run:
            networkmanager? ( kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}] )
    "
    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'networkmanager KF5NetworkManagerQt' )
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'lm_sensors Sensors'
    'network-usage libpcap'
)

if ever at_least 5.18.90 ; then
    # Needs dbus, fails on build.kde.org, too
    DEFAULT_SRC_TEST_PARAMS+=( -E "ksystemstatstest" )
fi

ksysguard_pkg_postinst() {
    if option network-usage ; then
        nonfatal edo setcap cap_net_raw+ep /usr/$(exhost --target)/libexec/${PN}/ksgrd_network_helper
    fi

    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

ksysguard_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

