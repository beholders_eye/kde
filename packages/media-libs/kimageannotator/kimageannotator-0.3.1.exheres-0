# Copyright 2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ksnip pn=kImageAnnotator tag=v${PV} ] \
    cmake

SUMMARY="Tool for annotating images"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

QT_MIN_VER="5.9.4"

# https://github.com/ksnip/kImageAnnotator/issues/127
RESTRICT="test"

DEPENDENCIES="
    build:
        x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = [ Qt5LinguistTools ] ]]
    build+run:
        media-libs/kcolorpicker[>=0.1.4]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]

"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_EXAMPLE:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_TESTS:BOOL=FALSE
    -DKIMAGEANNOTATOR_LANG_INSTALL_DIR:PATH=/usr/share/kImageAnnotator/translations
)

src_prepare() {
    cmake_src_prepare

    # TODO: Upstream, fix install path for translation
    edo sed \
        -e 's:${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}:${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i CMakeLists.txt
}

