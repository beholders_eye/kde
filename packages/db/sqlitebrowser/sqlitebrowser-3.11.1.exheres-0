# Copyright 2015 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Database browser for SQLite"
DESCRIPTION="
A high quality, visual, open source tool to create, design, and edit database files compatible with
SQLite.
"
HOMEPAGE+=" https://sqlitebrowser.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# FIXME: Interal copy of antlr, qhexedit2 and qcustomplot (Fedora has a patch)
DEPENDENCIES="
    build:
        x11-libs/qttools:5 [[ note = [ Qt5LinguistTools ] ]]
    build+run:
        app-editors/QScintilla
        dev-db/sqlite:3
        x11-libs/qtbase:5[gui]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.11.1-unbundle.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_STABLE_VERSION:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTING:BOOL=TRUE -DENABLE_TESTING:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream
    edo sed \
        -e 's:DESTINATION share:DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i CMakeLists.txt
}

src_install() {
    cmake_src_install

    insinto /usr/share/icons/hicolor/scalable/apps
    doins "${CMAKE_SOURCE}"/images/${PN}.svg
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

